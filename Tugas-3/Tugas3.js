// Soal 1
var kataPertama = "saya";
var kataKedua = "senang";
var kataKetiga = "belajar";
var kataKeempat = "javascript";

// ... Jawaban Soal 1
console.log(kataPertama.concat(kataKedua).concat(kataKetiga).concat(kataKeempat));
console.log(kataPertama + " " + kataKedua.charAt(0).toUpperCase()+kataKedua.slice(1) + " " + kataKetiga + " " + kataKeempat.toUpperCase());

// Soal 2
var kataPertama = "1";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "5";

// ... Jawaban Soal 2
console.log(parseInt(kataPertama)+parseInt(kataKedua)+parseInt(kataKetiga)+parseInt(kataKeempat));

// Soal 3
var kalimat = "Wah Javascript itu keren sekali";

var kataPertama = kalimat.substring(0,3);
var kataKedua = kalimat.substring(4,14);
var kataKetiga = kalimat.substring(15,18);
var kataKeempat = kalimat.substring(19,24);
var kataKelima = kalimat.substring(25,31);

// ... Jawaban Soal 3
console.log('kataPertama: ' + kataPertama);
console.log('kataKedua: ' + kataKedua);
console.log('kataKetiga: ' + kataKetiga);
console.log('kataKeempat: ' + kataKeempat);
console.log('kataKelima: ' + kataKelima);

// Soal 4
var nilai;
nilai = 75;

// ... Jawaban Soal 4
if (nilai >= 80 ) 
{
    console.log('Indeks : A');
}
else if (nilai >= 70 && nilai < 80)
{
    console.log('Indeks : B');
}
else if (nilai >= 60 && nilai < 70)
{
    console.log('Indeks : C');
}
else if (nilai >= 50 && nilai < 60)
{
    console.log('Indeks : D');
}
else if (nilai < 50)
{
    console.log('Indeks : E');
}


// Soal 5
var tanggal = 22;
var bulan = 7;
var tahun = 2020;

// ... Jawaban Soal 4
switch (bulan)
{
    case 1: { bulan = 'Januari'; break; }
    case 2: { bulan = 'Februari'; break; }
    case 3: { bulan = 'Maret'; break; }
    case 4: { bulan = 'April'; break; }
    case 5: { bulan = 'Mei'; break; }
    case 6: { bulan = 'Juni'; break; }
    case 7: { bulan = 'Juli'; break; }
    case 8: { bulan = 'Agustus'; break; }
    case 9: { bulan = 'September'; break; }
    case 10: { bulan = 'Oktober'; break; }
    case 11: { bulan = 'November'; break; }
    case 12: { bulan = 'Desember'; break; }
}

console.log(tanggal + " " + bulan + " " + tahun);