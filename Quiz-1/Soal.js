

// Soal 11
var daftarNama = [];

function tambahNama(nama, jenisKelamin) {
    var panggilan;
    if (jenisKelamin == "LakiLaki")
    {
        panggilan = "Bapak";
    }
    else if (jenisKelamin == "Perempuan")
    {
        panggilan = "Ibu";
    }
    daftarNama.push({
        nama: nama,
        jenisKelamin: jenisKelamin,
        panggilan: panggilan,
    });
}

tambahNama("Asep", "LakiLaki");
tambahNama("Siti", "Perempuan");
tambahNama("Yeni", "Perempuan");
tambahNama("Rudi", "LakiLaki");
tambahNama("Adit", "LakiLaki");

daftarNama.forEach(function(item, index){
    console.log(index + 1 + ". " + item.panggilan + " " + item.nama);
})


// Soal 12
// Luas Lingkaran
function luasLingkaran(radius)
{
    const phi = 3.14;
    return 0.5 * radius * radius * phi;
}

// Luas Segitiga
function luasSegitiga(panjang, lebar)
{
    return panjang * lebar / 2 ;
}

// Luas Persegi
function luasPersegi(panjang)
{
    return panjang*panjang;
}

console.log(luasLingkaran(7));
console.log(luasSegitiga(7,7));
console.log(luasPersegi(7));

// Soal 13
var daftarAlatTulis = ["2. Pensil", "5. Penghapus", "3. Pulpen", "4. Penggaris", "1. Buku"];
daftarAlatTulis.sort();

var i=0;
while(i < daftarAlatTulis.length)
{
    console.log(daftarAlatTulis[i]);
    i++;
}

// Soal 14
var pesertaLomba = [
    ["Budi", "Pria", "172cm"],
    ["Susi", "Wanita", "162cm"],
    ["Lala", "Wanita", "155cm"],
    ["Agung", "Pria", "175cm"],
];

pesertaLomba = pesertaLomba.map(function(item){
    return ({
        nama: item[0],
        jkel: item[1],
        tinggi: item[2],
    });
});

console.log(pesertaLomba);