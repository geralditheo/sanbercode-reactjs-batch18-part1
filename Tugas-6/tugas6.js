console.log("---Soal 1---");
// ... Jawaban Soal 1

const kelilingLingkaran = (radius) =>
{
    const phi = 3.14;
    return 2 * phi * radius;
}

const luasLingkaran = (radius) =>
{
    const phi = 3.14;
    return 0.5 * phi * radius * radius;
}

console.log(kelilingLingkaran(10));
console.log(luasLingkaran(10));

console.log("-----------------\n")

console.log("---Soal 2---");

let kalimat = "";
// ... Jawaban Soal 2

const addKata = (kata) =>
{
    kalimat = `${kalimat} ${kata}`;
}

addKata("Saya");
addKata("Adalah");
addKata("Seorang");
addKata("FrontEnd");
addKata("Developer");

console.log(kalimat);

console.log("-----------------\n")

console.log("---Soal 3---");
// ... Jawaban Soal 3

const newFunction = (firstName, lastName) => {
    return {
        fullName:  () => {
            console.log(firstName + " " + lastName);
            return
        }
    }
}



newFunction("WIlliam", "Imoh").fullName();

console.log("-----------------\n")

console.log("---Soal 4---");

const newObject = 
{
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwart React Conf",
    occupation: "Deve-wizard avocado",
    spell: "Vimulus Renderus"
}

// const firstName = newObject.firstName;
// const lastName = newObject.lastName;
// const destination = newObject.destination;
// const occupation = newObject.occupation;

// ... Jawaban Soal 4

const {firstName, lastName, destination, occupation} = newObject;

console.log(firstName, lastName, destination, occupation);

console.log("-----------------\n")

console.log("---Soal 5---");

const west = ["Will", "Chris", "Sam", "Holly"];
const east = ["Gill", "Brian", "Noel", "Maggie"];
// const combined = west.concat(east);
// ... Jawaban Soal 5

const combined = [...west, ...east];

console.log(combined);
console.log("-----------------\n")