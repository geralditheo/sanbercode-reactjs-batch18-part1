// Soal 1
// ... Jawaban Soal 1

// versi 1
console.log("LOOPING PERTAMA");
var i = 2;
while (i <= 20)
{
    console.log(i + ' - '+'I love coding');
    i += 2;
}

console.log("LOOPING KEDUA");
i = 20;
while ( i > 0)
{
    console.log(i + ' - '+'I will become a front end developer');
    i -= 2;
}

// versi 2
var i = 2;
var hitung = "maju";

while(true)
{
    if( hitung == "maju")
    {
        if (i == 2)
        {
            console.log("LOOPING PERTAMA");
        }
        if (i <= 20)
        {
            console.log(i + " - I love coding");
        }
        else
        {
            i -= 2*2;
            hitung = "mundur";
        }
        i+=2;
    }
    else if (hitung == "mundur")
    {
        if (i == 20)
        {
            console.log("LOOPING KEDUA");
        }
        if(i >= 2)
        {
            console.log(i + " - I will become front end developer");
        }
        else{
            break;
        }
        i-=2;
    }
}

// Soal 2
// ... Jawaban Soal 2
var j;

for (j = 1; j <= 20; j += 1)
{
    if(j % 3 == 0 && j % 2 == 1)
    {
        console.log(j + ' - I Love Codding');
    }
    else if (j % 2 == 1)
    {
        console.log(j + ' - Santai');
    }
    else if (j % 2 == 0)
    {
        console.log(j + ' - Berkualitas');
    }
}

// Soal 3
// ... Jawaban Soal 3
var x = 7;
var y = x;

// Versi satu
var pagar = ['#'];
do 
{    
    pagar = pagar.join("");
    console.log(pagar);

    pagar = pagar.split("");
    pagar.push('#');

    x -= 1;
}while(x >= 1 );

// Versi dua

x = 7;

do
{   
    var pagar = '';
    y = 7;
    do
    {
        pagar = pagar + '#';
        y--;
    }while(y >= x)    
    console.log(pagar);
    x--;
}while(x >= 1)



// Soal 4
var kalimat = "saya sangat senang belajar javascript";
// ... Jawaban Soal 4
kalimat = kalimat.split(" ");
console.log(kalimat);



// Soal 5
var daftarBuah = ["2. Apple", "5. Jeruk", "3. Anggur", "4. Semangka", "1. Mangga"];
// ... Jawaban Soal 5
daftarBuah.sort();
console.log(daftarBuah);

i = 0;
while (i < daftarBuah.length)
{
    console.log(daftarBuah[i]);
    i++;
}