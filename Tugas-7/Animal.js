console.log("-------------SOAL 1 ---------------------");
console.log("-----------Realese 0------------");
class Animal
{
    constructor(name, legs=4, cold_blooded=false)
    {
        this._name = name;
        this._legs = legs;
        this._cold_blooded = cold_blooded;
    }
    get name()
    {
        return this._name;
    }
    get legs()
    {
        return this._legs;
    }
    get cold_blooded()
    {
        return this._cold_blooded;
    }
}

var sheep = new Animal("Shawn");

console.log(sheep.name);
console.log(sheep.legs);
console.log(sheep.cold_blooded);

console.log("-----------Realese 1------------");

class Frog extends Animal
{
    constructor(name)
    {
        super(name);
    }
    jump()
    {
        console.log("Hop Hop")
    }
}

class Ape extends Animal
{
    constructor(name)
    {
        super(name);
        this._legs = 2;
    }
    yell()
    {
        console.log("Auooo");
    }
}

var sungokong  = new Ape("Kera Sakti");
console.log(sungokong.name);
console.log(sungokong.legs);
console.log(sungokong.cold_blooded);
sungokong.yell();

console.log("----------------------------------------");

var kodok = new Frog("Buduk");
console.log(kodok.name);
console.log(kodok.legs);
console.log(kodok.cold_blooded);
kodok.jump();
console.log("----------------------------------------");    