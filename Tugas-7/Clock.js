// function Clock ({template})
// {
//     var timer;

//     function render ()
//     {
//         var date = new Date();

//         var hours = date.getHours();
//         if (hours < 10) {hours = '0' + hours}

//         var mins = date.getMinutes();
//         if (mins < 10) {mins = '0' + mins}

//         var secs = date.getSeconds();
//         if (secs < 10) {secs = '0' + secs}

//         var output = template
//             .replace('h', hours)
//             .replace('m', mins)
//             .replace('s', secs)

//         console.log(output);
//     }

//     this.stop = function()
//     {
//         clearInterval(timer);
//     }
    
//     this.start = function()
//     {
//         render();
//         timer = setInterval(render, 1000);
//     }    
// }

class Clock
{
    constructor({template})
    {
        this._template = template;
        this._timer;
    }

    render()
    {
        this._date = new Date();

        this._hours = this._date.getHours();
        if (this._hours < 10) {this._hours = '0' + this._hours};

        this._mins = this._date.getMinutes();
        if (this._mins < 10) {this._mins = '0' + this._mins};

        this._secs = this._date.getSeconds();
        if (this._secs < 10) {this._secs = '0' + this._secs};

        this._output = this._template
            .replace('h', this._hours)
            .replace('m', this._mins)
            .replace('s', this._secs)
            

        console.log(this._output);
    }

    stop = () => {
        console.log("stop");
        clearInterval(this._timer);
    }

    start = () => {
        console.log("Start");
        this.render();
        this._timer = setInterval(() => this.render(), 1000);
    }


}

var clock = new Clock({template: 'h:m:s'});
clock.start();