// Soal 1
// .... Jawaban Soal 1

function halo() {
    return "Halo Sanbers";
}

console.log(halo());

// Soal 2
// .... Jawaban Soal 2

function kalikan(num1, num2) {
    return num1 * num2;
}

var num1 = 12;
var num2 = 4;

var hasilKali = kalikan(num1, num2);
console.log(hasilKali);

// Soal 3
// .... Jawaban Soal 3

function introduce(name, age, address,hobby) {
    return "Nama saya " + name + ", umur saya " + age + " tahun, alamat saya di " + address + ", saya punya hobby yaitu " + hobby;
}

var name = "Geraldi";
var age = 22;
var address = "Jalan Jalan";
var hobby = "Learn Javascript";

var perkenalan = introduce(name, age, address, hobby);
console.log(perkenalan);

// Soal 4
var arrayDaftarPeserta = ["Asep", "Laki- Laki", "Baca Buku", 1992];

// .... Jawaban Soal 4

arrayDaftarPeserta = {
    nama: arrayDaftarPeserta[0],
    "jenis kelamin": arrayDaftarPeserta[1],
    hobi: arrayDaftarPeserta[2],
    "tahun lahir": arrayDaftarPeserta[3]
}


console.log(arrayDaftarPeserta);

// Soal 5

// .... Jawaban Soal 5

var dataArrayOfObject = [
    {
        nama: "strawberry",
        warna: "merah",
        harga: 9000,
        "ada bijinya": "tidak",
    },
    {
        nama: "jeruk",
        warna: "jingga",
        harga: 8000,
        "ada bijinya": "ada",
    },
    {
        nama: "Semangka",
        warna: "Hijau & Merah",
        harga: 10000,
        "ada bijinya": "ada",
    },
    {
        nama: "Pisang",
        warna: "kuning",
        harga: 5000,
        "ada bijinya": "tidak",
    }
];

console.log(dataArrayOfObject[0]);

// Soal 6

var dataFilm = [];

// .... Jawaban soal 6

function addDataFilm(data) {
    data.push({
        nama: "Pirates Of Carribean",
        durasi: 5 + " jam",
        genre: "Adventure",
        tahun: 2010
    });
}

addDataFilm(dataFilm);
console.log(dataFilm);