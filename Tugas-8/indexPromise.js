console.log("-------------------SOAL 2---------------------");

let readBookPromise = require('./promise.js');

let books = 
[
    {name: "LOTR", timeSpent: 3000},
    {name: "Fidas", timeSpent: 2000},
    {name: "Kalkulus", timeSpent: 4000},
    // {name: 'Komik', timeSpent: 2000}
]

let bookIndex = 0;
let timeRead = 10000;

const howMuchBooksIRead = (time) => 
{
    if ( books[bookIndex] )
    {
        readBookPromise(time, books[bookIndex])
            .then((result) => 
            {
                console.log(result + " result");
                time = result;
                bookIndex++;
                howMuchBooksIRead(time);
            })
            .catch((err) => 
            {
                console.log(err + " err");
            });
    }
    else
    {
        console.log("End");
    }
}

howMuchBooksIRead(timeRead);

